public class JavaBooleans4 {
    public static void main(String[] args) {
        int x = 10;
        System.out.println(x == 10);  // return true, because the value of x is equal to 10
    }
}
